@extends('layouts.admin')
@section('title','Gestion des commandes')
@section('content')
    
   <h1>Liste des commandes</h1>
   <a class="aff btn btn-success mb-5" href="{{route('commandes.index')}}">Afficher tous les commandes</a>
   <div class='d-flex justify-content-around'>
   <form class="formtt" action="{{route('commandes.index')}}">
    <div class="torigth">
        <label for="etat">Etat</label>
    
    <select class="mb-3 form-control" name="etat" id="etat">
      <option disabled selected value>Selectioner un etat </option>
      @foreach($etats as $t)
      @if($t->id == $et)
      <option selected="selected" value="{{$t->id}}">{{$t->intitule}}</option>
      @else
      <option value="{{$t->id}}">{{$t->intitule}}</option>
      @endif
      @endforeach
    </select>
  </div>
  
  
  <div>
    <label for="nom">Nom</label>
    <input type="text" name="nom" label="nom">
    <input type="submit" value="Rechercher">
  </div>

</form>

<form action="{{ route('exportcsv') }}" method="GET">
  <button class="btn btn-primary" type="submit">Export CSV</button>
</form>
</div>
   <table id="tbl">
     <tr>
        <th>Id</th>
         <th>Etat</th>
       <th>Client</th>
       <th>Date</th>
       <th>Update</th>
       <th colspan="3">Actions</th>
     </tr>
     @foreach ($commandes as $c)
     <tr >
            <td >{{$c->id}}</td>
            <td><span style="color:white; border-radius: 10px; padding: 5px 10px; display: inline-block;background-color:@if($c->etat->id == 1)
              red
          @elseif($c->etat->id == 2)
              orange
          @elseif($c->etat->id == 3)
              yellow
          @elseif($c->etat->id == 4)
              green
          @elseif($c->etat->id == 5)
              blue
          @else
              white
          @endif">{{$c->etat->intitule}}</span>
           <td>{{$c->client->nom.' '.$c->client->prenom}}</td>
           <td>{{$c->date}}</td>
           <td>
            <form class="d-flex" action="{{route('etat.change',["id"=>$c->id])}}" method="POST">
            @csrf
              <select class="mb-3 form-control" name="etatt" id="etatt">
                <option disabled selected value>Selectioner un etat </option>
                @foreach($etats as $t)
                @if($t->id > $c->etat->id and $c->etat->id != 4)
                <option value="{{$t->id}}">{{$t->intitule}}</option>
                @elseif($c->etat->id == 4)
                
                @endif
                @endforeach
              </select>
            <input type="submit" value="Change">
            </form>
           </td>
           <td><a class="btn btn-info" href="{{route('commandes.show',["commande"=>$c->id])}}">Details</a></td>
         </tr>
     @endforeach
   </table>
@endsection