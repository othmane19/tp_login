@extends('layouts.admin')
 @section('title','Detail d \'une commande')
 @section('content')
    <a href="{{route('commandes.index')}}">Retourner vers la liste des commandes</a>
    <h1>Detail de la commande Num {{$c->id}}</h1>
    <div>
        <p><strong>Etat:</strong> {{$c->etat->intitule}}</p>
        <p><strong>Client:</strong> {{$c->client->nom}}</p>
        <p><strong>Date:</strong> {{$c->date}}</p>
        @foreach($c2 as $cc)
        <p><strong>Produit:</strong> {{$cc->produit->designation}}</p>
        <p><strong>Quantite:</strong> {{$cc->quantite}}</p>
        @endforeach
        <p><strong>Prix Total:</strong> {{$pt}}</p>
        
        

    </div>
@endsection