<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <title>@yield('title','App store')</title>
</head>
<body>
    <div class="wrapper">
    <div class='d-flex justify-content-between'>
    <nav class="navbar navbar-expand-sm">
        <ul class="navbar-nav">
            <li class="nav-item"><a class="nav-link" href="{{route('home.index')}}">Catalogue</a></li>
            <li class="nav-item"><a class="nav-link " href="{{route('categories.index')}}">Gestion des categories</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('produits.index')}}">Gestion des produits</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('home.panier')}}">Mon panier</a></li>
            <li class="nav-item"><a class="nav-link" href="{{route('commandes.index')}}">Commandes</a></li>
        </ul>
    </nav>
    @auth
    <div >
    <form method="POST" action="{{ route('logout') }}">
        @csrf
        <input type="submit" class="btn btn-primary" value="Logout"></button>
    </form>
    @endauth
</div>
</div>
    <div class="main">
        @yield('content')
    </div>
</div>
    <footer class="footer">
        &copy;OFPPT 2024
    </footer>
</body>
</html>
