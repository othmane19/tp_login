 @extends('layouts.admin')
 @section('title','Gestion des produits')
 @section('content')
     
    <h1>Liste des produits</h1>
    <a class="btn btn-primary mb-3" href="{{route('produits.create')}}">Ajouter un nouveau produit</a>
    <br>
    <div class="searsh">
      <form action="{{route('produits.index')}}" method="GET">
        @csrf
        <label for="designation">designation :</label>
        <input type="text" name="designation" id="designation">
        <label for="min">prix min :</label>
        <input type="number" step="any" name="min" id="min">
        <label for="max">prix max :</label>
        <input type="number" step="any" name="max" id="max">
        <label for="quantite_stock">quantite stock :</label>
        <input type="number" name="quantite_stock" id="quantite_stock">
        <label for="cate">Categeorie</label>
        <select id="cate" name="cate">
        @foreach($categories as $cat)
          <option value="{{$cat->id}}">{{$cat->designation}}</option>
        @endforeach
        <input class="mt-4" type="submit" value="Filter">
 
        </select>
      </form>
    </div>
    <br>
    <table class="table table-stripped" id="tbl">
      <tr>
          <th>Id</th>
        <th>Designation</th>
        <th>prix</th>
        <th>quantite stocke</th>
        <th>categorie</th>
        <th colspan="3">Actions</th>
      </tr>
      @foreach ($produits as $prod)
          <tr>
            <td>{{$prod->id}}</td>
            <td>{{$prod->designation}}</td>
            <td>{{$prod->prix_u}}</td>
            <td>{{$prod->quantite_stock}}</td>
            <td>{{$prod->categorie->designation}}</td>
            <td><a href="{{route('produits.show',["produit"=>$prod->id])}}">Details</a></td>
            <td><a href="{{route('produits.edit',["produit"=>$prod->id])}}">Modifier</a></td>
            <td>
                <form action="{{route('produits.destroy',["produit"=>$prod->id])}}" method="POST">
                    @method('DELETE')
                    @csrf
                    <input type="submit" value="Supprimer" onclick="return confirm('voulez-vous supprimer ce produit?')">
                </form></td>
          </tr>
      @endforeach
    </table>
{{ $produits->links() }}

 @endsection