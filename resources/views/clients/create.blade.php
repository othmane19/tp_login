@extends('layouts.admin')
 @section('title','Enregistrer infos client')
 @section('content')
    <h1>Vos informations </h1>
    <form action="{{route('commandes.store')}}" method="POST">
        @csrf
        <div>
        <label for="nom">Nom</label>
        <input type="text" name="nom" id="nom" value="{{old('nom')}}">
        </div>
        <div>
        <label for="prenom">Prenom</label>
        <input type="text" name="prenom" id="prenom" value="{{old('prenom')}}">
        </div>
        <div>
        <label for="tele">Tele</label>
        <input type="text" name="tele" id="tele" value="{{old('tele')}}">
        </div>
         
        <div>
            <input type="submit" value="Ajouter">
        </div>
    </form>
    <div>
        @if($errors->any())
        <ul>
          @foreach($errors->all() as $er)
           <li>{{$er}}</li>
           
           @endforeach
        </ul>
         


        @endif
    </div>
@endsection