@extends('layouts.admin')
@section('title','Ajouter un produit')
@section('content')
@guest

<form method="POST" action="{{ route('login') }}" class="w-50 mx-auto mt-5">
    @csrf
    
    <div class="form-outline mb-4">
    <label class="form-label" for="form2Example1">Email address</label>
      <input type="email" id="email" name="email" class="form-control" />
      
    </div>
  
    
    <div class="form-outline mb-4">
        <label  class="form-label" for="form2Example2">Password</label>
      <input type="password" id="password" name="password" class="form-control" />
      
    </div>
  
    
    <div class="row mb-4">
      <div class="col d-flex justify-content-center">
        
        <div class="form-check">
          <input class="form-check-input" type="checkbox" value="" id="form2Example31" checked />
          <label class="form-check-label" for="form2Example31"> Remember me </label>
        </div>
      </div>
  
      <div class="col">
        
        <a href="#!">Forgot password?</a>
      </div>
    </div>
  
    
    <button type="submit" class="btn btn-primary btn-block mb-4">Sign in</button>
  
  </form>
    @else
    <p>Already Logged in <a href="{{ route('home.index') }}">Home</a></p>
@endguest
@endsection