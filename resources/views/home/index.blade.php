 @extends('layouts.client')
 @section('title','Gestion des categories')
 @section('content')
     
    <h1>Liste des produits</h1>
    <div class="container mx-auto row g-3">
   
   @foreach ($produits as $item)
       <div class="card col-sm-3">
        <img src="{{asset('storage/'.$item->image)}}" class="card-img-top">
        <div class="card-body">
        <p>{{$item->designation}}</p>
        <p>Prix : {{$item->prix_u}} MAD</p>
        @if ($item->quantite_stock==0)
            <p>En repture de stock</p>
        @else
        <p>En stock : {{$item->quantite_stock}}</p>
        <form action="{{route('home.add',["id"=>$item->id])}}" method='POST'>
            @csrf
            <label for="qte">Quantite</label>
            <input type="number" name="qte" id="qte" min="1" max="{{$item->quantite_stock}}">
            <input class="btn btn-primary" type="submit" value="Acheter">
        </form>

          @endif
        </div>
       </div>
 
   @endforeach
     
    </div>

@endsection