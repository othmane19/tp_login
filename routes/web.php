<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\EtatController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClientController;
use App\Http\Controllers\ProduitController;
use App\Http\Controllers\CommandeController;
use App\Http\Controllers\CategorieController;
use App\Http\Controllers\CsvExportController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/',[HomeController::class,"index"])->name('home.index');
Route::get('/panier',[HomeController::class,"show_panier"])->name('home.panier');
Route::get('/panier/clear',[HomeController::class,"clear"])->name('home.clear');
Route::post('panier/add/{id}',[HomeController::class,"add"])->name('home.add');
Route::delete('panier/delete/{id}',[HomeController::class,"delete"])->name('home.delete');
Route::resource("clients",ClientController::class);
Route::resource("commandes",CommandeController::class);
Route::middleware(['auth'])->group(function () {
    

    Route::resource("categories",CategorieController::class);
    Route::resource("produits",ProduitController::class);
    Route::post('/logout', [LoginController::class,'logout'])->name('logout');
    Route::get('/register', [RegisterController::class,'showRegistrationForm']);
    Route::post('/register', [RegisterController::class,'register'])->name('register');
    Route::post('/commandes/{id}/etat',[EtatController::class,'changeEtat'])->name('etat.change');
    Route::get('/export-csv', [CsvExportController::class, 'export'])->name('exportcsv');
    
});

Route::get('/login',[LoginController::class,'showLoginForm']);
Route::post('/login', [LoginController::class,'login'])->name('login');



