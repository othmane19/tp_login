<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Etat extends Model
{
    use HasFactory;
    protected $fillable= ["id","intitule","description"];
    public function commandes(){
        return $this->hasMany(Commande::class);
    }
}
