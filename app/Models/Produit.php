<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    protected $fillable=['designation','prix_u','quantite_stock','categorie_id','image'];
    public function categorie(){
        return  $this->belongsTo(Categorie::class);
     }
}
