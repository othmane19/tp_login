<?php

namespace App\Models;

use App\Models\Etat;
use App\Models\LigneDeCommande;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Commande extends Model
{
    use HasFactory;
    protected $fillable=['date','client_id','etat_id'];

    public function produits(){
        return $this->belongsToMany(Produit::class)->using(LigneDeCommande::class);
    }
    public function client(){
        return $this->belongsTo(Client::class);
    }
    public function etat(){
        return $this->belongsTo(Etat::class);
    }
}
