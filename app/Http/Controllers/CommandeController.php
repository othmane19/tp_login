<?php

namespace App\Http\Controllers;
use App\Models\Etat;

use App\Models\Client;
use App\Models\Produit;
use App\Models\Commande;
use Illuminate\Http\Request;
use App\Models\LigneDeCommande;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $etats = Etat::all();
        $nom = $request->query('nom');
        $et = $request->query('etat');
        $q = Commande::query();
        $commandes=Commande::all();
        if($et){
            $q->where('etat_id','like',$et);
        }
        if($nom){
            $q->whereHas("client",function($query)use($nom){
                $query->where('nom','like','%'.$nom.'%')->orWhere('prenom','like','%'.$nom.'%')
                ;
            
            });
        };
        $commandes = $q->get();
        return view("commandes.index",compact('commandes','et','etats'));
    
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $v=$request->validate([
            'nom'=>'required',
            'prenom'=>'required',
            'tele'=>'required'
            
        ]);
        $client=Client::create($v);
        $cmd=Commande::create(['date'=>now(),'client_id'=>$client->id,'etat_id'=>1]);
        $panier=$request->session()->get('panier');
        foreach($panier as $id=>$item){
            LigneDeCommande::create([
                'produit_id'=>$item['produit']->id,
                'commande_id'=>$cmd->id,
                'quantite'=>$item['qte'],
                'prix'=>$item['produit']->prix_u,
            ]);
            $produit=Produit::find($item['produit']->id);
            $produit->quantite_stock-=$item['qte'];
            $produit->save();
        }
        return view('commandes.succes');


    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $c=Commande::find($id);
        $c2 = LigneDeCommande::where('commande_id','like', $id)->get();
        $pt = 0;
        foreach($c2 as $p){
            $pt += $p->quantite*$p->prix;
        }
        return view('commandes.show')->with(["c"=>$c,"c2"=>$c2,"pt"=>$pt]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
