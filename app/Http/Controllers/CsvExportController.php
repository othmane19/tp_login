<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

class CsvExportController extends Controller
{
    public function export()
    {
        $data = Commande::all();


        $filename = 'commandes_export_' . Str::slug(now()) . '.csv';

        $csvFile = fopen(storage_path('/commande_csv/' . $filename), 'w');

        
        fputcsv($csvFile, ['Id', 'Etat','Nom','Telephone','Date']);

        
        foreach ($data as $row) {
            fputcsv($csvFile, [$row->id,$row->etat->intitule,$row->client->nom.' '.$row->client->prenom,$row->client->tele,$row->date]); 
        }

        fclose($csvFile);
        return response()->download(storage_path('/commande_csv/' . $filename))->deleteFileAfterSend(true);
    }
}