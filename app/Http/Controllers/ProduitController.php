<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use App\Models\Categorie;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    //
        /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {

        $categories = Categorie::all();
        $des=$request->query("designation");
        $max=$request->query("max");
        $min=$request->query("min");
        $cate = $request->query('cate');
        $quantite_stock=$request->query("quantite_stock");
        if($des){
            $q=Produit::where('designation','like',"%".$des."%");
        }
        elseif ($max){
            $q=Produit::where('prix_u','<=',$max);
        }else if ($min) {
            $q=Produit::where('prix_u','>=',$min);
        }
        else if ($quantite_stock) {
            $q=Produit::where("quantite_stock","like","%".$quantite_stock."%");
        } 
        else if($cate){
            $q=Produit::where('categorie_id','like',$cate);
        }
        else {
            $q=Produit::query();
        }
        $produits=$q->paginate(10);
        return view("produits.index",compact("produits","categories"));
    }
    
    /**
     * Show the form for creating a new resource.
     */
    public function create()
    { 
        $categories=Categorie::all();
        return view('produits.create',compact("categories"));
        
        
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        // $cat=new Categorie();
        // $cat->designation=$request->input('designation');
        // $cat->description=$request->input('description');
        // $cat->save();
        $validateData = $request->validate([
            'designation'=>'required|unique:produits,designation',
            'prix_u'=>'required',
            'quantite_stock'=>'required',
            'categorie_id'=>'required',
            "image"=>"image|mimes:jpeg,png,jpg,gif,svg"
        ]);
        $imagePath = $request->file("image")->store("products/image","public");
        $validateData["image"] = $imagePath;
        Produit::create($validateData);
        return  redirect()->route('produits.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $prod=Produit::find($id);
        
        
        return view('produits.show')->with("prod",$prod);
    }

    /**
     * Show the form for editing the specified resource.
     */ 
    public function edit(string $id)
    {
        $prod=Produit::find($id);
        $categories = Categorie::all();
        return view('produits.edit',compact('prod',"categories"));
        }

    

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
          $validateData = $request->validate([
            'designation'=>'required|unique:produits,designation,'.$id,
            'prix'=>'required',
            'quantite_stock'=>'required',
            "image"=>"image|mimes:jpeg,png,jpg,gif,svg"
        ]);
        if($request->hasFile("image")) {
            $imagePath = $request->file("image")->store("products/image","public");
            $validateData["image"] = $imagePath;
        }
        $prod=Produit::find($id);
        $prod->update($validateData);
        return redirect()->route('produits.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        Produit::destroy($id);
        return  redirect()->route('produits.index');

    }


}
