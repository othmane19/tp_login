<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use Illuminate\Http\Request;

class EtatController extends Controller
{
    public function changeEtat(Request $request,$id){
        $etat = $request->input('etatt');
        $commande=Commande::find($id);
        $commande->update(["etat_id"=>$etat]);
        return redirect()->route('commandes.index');
    }
}
